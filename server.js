const next = require('next');
const expressApp = require('./express-app');

// ################################################################
// APP AND SERVER CONFIG
// ################################################################
const port = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();

nextApp
  .prepare()
  .then(() => {
    expressApp.get('*', (req, res) => handle(req, res));
    expressApp.listen(port, err => {
      if (err) throw err;
      console.log(`> Ready on ${port}`);
    });
  })
  .catch(ex => {
    process.exit(1);
    console.error(ex.stack);
  });
