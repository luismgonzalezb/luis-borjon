import React from 'react';

import './Logo.scss';

const Logo = () => (
  <h1 className="logo">
    <span>Luis</span>
    <span className="second active">Gonzalez</span>
    <span className="second">Borjon</span>
  </h1>
);

export default Logo;
