import React, { Component } from 'react';
import cx from 'classnames';
import Logo from './Logo';
import MouseLink from '../components/MouseLink';
import { getElementXOffset } from '../lib/positionUtils';
import './Menu.scss';

const options = [
  { id: 1, label: 'Home', url: '#hero' },
  { id: 2, label: 'Community', url: '#community' },
  { id: 3, label: 'Work', url: '#work' },
  { id: 4, label: 'Contact', url: '#contact' },
  { id: 5, label: 'Fun', url: '#fun' },
];

class Menu extends Component {
  state = {
    option: 1,
  };
  onSelect = (evt, option) => {
    const x = getElementXOffset(option.url.substr(1));
    console.log(x);
    window.scroll({
      top: x,
      left: 0,
      behavior: 'smooth',
    });
    this.setState({ option: option.id });
    evt.preventDefault();
    evt.stopPropagation();
  };
  onMouseEnter = option => {
    const event = new CustomEvent('open-link', { detail: option });
    window.dispatchEvent(event);
  };
  render() {
    const { option } = this.state;
    return (
      <header className="header">
        <Logo />
        <nav className="menu menu--alonso">
          <ul className="menu__list">
            {options.map(opt => (
              <li
                key={opt.id}
                className={cx('menu__item', {
                  'menu__item--current': opt.id === option,
                })}
              >
                <MouseLink
                  url={opt.url}
                  label={opt.label}
                  onClick={evt => this.onSelect(evt, opt)}
                  className="menu__link"
                />
              </li>
            ))}
            <li className="menu__line" />
          </ul>
        </nav>
      </header>
    );
  }
}

export default Menu;
