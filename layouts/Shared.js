import React, { Component } from 'react';
import PorpTypes from 'prop-types';
import Head from 'next/head';

import './Shared.scss';

class Shared extends Component {
  static propTypes = {
    device: PorpTypes.string.isRequired,
  };
  componentDidMount() {
    window.addEventListener('mousemove', this.moveCustomCursor);
    window.addEventListener('open-link', this.onMouseEnterLink);
    window.addEventListener('close-link', this.onMouseLeavesLink);
  }
  onMouseEnterLink = () => {};
  onMouseLeavesLink = () => {};
  moveCustomCursor = evt => {
    this.cursor.style.top = `${evt.clientY}px`;
    this.cursor.style.left = `${evt.clientX}px`;
  };
  render() {
    const { device } = this.props;
    return [
      <Head key="custom-head">
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width"
          key="viewport"
        />
        <link rel="stylesheet" href="/static/css/bundle.css" />
      </Head>,
      <div
        key="custom-cursor"
        className={`custom-cursor ${device}`}
        ref={ref => {
          this.cursor = ref;
        }}
      />,
    ];
  }
}

export default Shared;
