import React from 'react';
import PropTypes from 'prop-types';
import Shared from './Shared';
import Menu from './Menu';

import './Landing.scss';

const Landing = ({ children, device }) => [
  <Shared key="shared" device={device} />,
  <div key="main-content" className="scroll-content">
    <Menu />
    <div className="container">{children}</div>
  </div>,
];

Landing.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  device: PropTypes.string.isRequired,
};

export default Landing;
