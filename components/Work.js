import React from 'react';

import './Work.scss';

const Work = () => (
  <section id="work" className="section work">
    <h2>Work</h2>
    <div>
      <div>Trulia</div>
      <div>Senior Software Engineer</div>
      <div>Nov 2017 – Present</div>
      <div>
        <p>
          Joined the Mortgages team to build tools that help and empower people
          to make one of the most important purchases on their life easier and
          more transparent.
        </p>
      </div>
    </div>
    <div>
      <div>Glassdoor</div>
      <div>Senior Software Engineer</div>
      <div>Jun 2015 – Oct 2017</div>
      <div>
        <p>
          I’ve had the opportunity to work on the B2B team, creating new
          products for the self-serve platform, that has grown more than 200%
          year over year, as well as tools for customers to manage their
          Glassdoor profile and products.
        </p>

        <p>
          Functioning as a reference for JavaScript and React, I was one of the
          2 main implementers of this new technology with in Glassdoor, focusing
          heavily on code and product quality, helping to create and enforce
          best practices for the technology such as Unit testing and conde
          linting. Also part of an initiative to migrate from JSP to Node.js
          that is ongoing.
        </p>
      </div>
    </div>
    <div>
      <div>Yahoo</div>
      <div>Senior Software Engineer</div>
      <div>Nov 2013 – Jun 2015</div>
      <div>
        <p>
          Working on the Home Page and Verticals group as Senior Software
          Engineer at one of the most recognized brands on Internet, part of the
          common experiences team creating components to be used across the
          company, focusing on native advertisement.
        </p>
        <p>
          With a great focus on performance as well as compatibility and
          accessibility, we have to take under consideration the all kinds of
          customers and their circumstances. Using the latest technologies and
          trends Node.js, React.js, HTML(5) among others.
        </p>
      </div>
    </div>
    <div>
      <div>Rockfish Interactive</div>
      <div>Sr. Developer</div>
      <div>Nov 2011 – Oct 2013</div>
      <div>
        <p>
          Senior Web Developer at an award winner digital agency, using cutting
          edge technologies to create Applications with the highest standards
          for web and mobile. Working with international customers such as
          Walmart/SAMS Club, Mead-Johnson, and nationwide brands as Corner
          Bakery, Value City Furniture, among others.
        </p>
        <p>
          I was lead and responsible for the Front-End development on the
          Mead-Johnson china site to be used as template for all international
          markets, a fully responsive site with cross browser compatibility
          concerns down to IE8, responsible for the Front-End development of the
          Walmart Corporate Brand Center redesign. Participated on several
          mobile micro site efforts for Walmart and Others. Responsible for
          updates and maintenance to the SAMS.com mobile site both on the front
          and back end, expert on .Net development (SOA, MVC, Entity-Framework,
          etc.) as well as knowledgeable on PHP. Part of Mitt Romney&#39;s
          presidential campaign team in charge of developing Mobile Apps (Flash
          Platform), Facebook Apps and web site, both on the Front and Back-End.{' '}
        </p>
      </div>
      <p>
        I as made part of the Rapid Innovations Team (RND and Swat). Praised for
        my versatility I had the opportunity to work on innovative technologies
        to push the market like Microsoft Kinect, the Corona SDK, PhoneGap,
        Adobe Flash and Flex, among others. Always exploring the newest tools
        and standards on the market to add value to the customer.
      </p>
    </div>
    <div>
      <div>Softtek</div>
      <div>Technical Leader</div>
      <div>dOct 2008 – Feb 2011</div>
      <div>
        <p>
          Support and Create applications for the ISD International Wal-Mart
          Division, I created the design of an entirely new architecture based
          on .Net for a new multinational and multilingual system for the
          Accounts Receivable area of Wal-Mart International. Leader of one of
          the biggest teams on the area with up to 10 developers. Coordinating
          the relationship with the customers. Working with scrum and waterfall.
          Involved on the entire life-cycle from requirements, design, estimate,
          develop, testing and deploy.
        </p>
      </div>
    </div>
  </section>
);

export default Work;
