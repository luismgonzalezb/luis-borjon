import React from 'react';

import './Hero.scss';

const Hero = () => (
  <section id="hero" className="section hero">
    <div className="row">
      <div className="col-sm">
        <img
          className="img-fluid"
          src="/static/img/luis-caricatura.png"
          alt="Luis Cartoon"
        />
      </div>
      <div className="col-sm">
        <h1 className="title">The Aha!! Moment</h1>
        <p className="copy">
          My passion is Software Engineering, I&#39;ve worked on this field for
          over 10 years and I still get surpriced and over joyed with the
          challenges and the soltions, I&#39;m addicted to the feeling of
          accomplishment when the &quot;aha!! moment&quot; arrives.
        </p>
      </div>
    </div>
  </section>
);

export default Hero;
