import React, { Component } from 'react';
import PropTypes from 'prop-types';

class MouseLink extends Component {
  onMouseEnter = event => {
    const custom = new CustomEvent('open-link', {
      detail: { target: event.target },
    });
    window.dispatchEvent(custom);
  };
  onMouseLeave = () => {
    const event = new CustomEvent('close-link');
    window.dispatchEvent(event);
  };
  render() {
    const { url, label, onClick, className } = this.props;
    return (
      <a
        href={url}
        onClick={onClick}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
        className={className}
      >
        {label}
      </a>
    );
  }
}

MouseLink.defaultProps = {
  className: '',
};

MouseLink.propTypes = {
  className: PropTypes.string,
  url: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default MouseLink;
