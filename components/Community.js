import React from 'react';
import MouseLink from './MouseLink';

import './Community.scss';

const Community = () => (
  <section id="community" className="section community">
    <h2>Sharing is the best way to grow, Need some help?</h2>
    <p>
      I&#39;ll be offering free time to students to work together and solve some
      questions and get some projects done.
    </p>
    <p>
      After a quick questionary you&#39;ll be able to schedule some time, thanks
      and talk to you soon
    </p>
    <div>
      <MouseLink url="/help" label="Let's get this Solved!" className="btn" />
    </div>
  </section>
);

export default Community;
