export const getElementXOffset = elementId => {
  const element = document.getElementById(elementId);
  return element.getBoundingClientRect().top;
};

export default getElementXOffset;
