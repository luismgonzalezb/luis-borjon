import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Landing from '../layouts/Landing';
import Hero from '../components/Hero';
import Community from '../components/Community';
import Work from '../components/Work';
import Contact from '../components/Contact';
import Fun from '../components/Fun';

import './Index.scss';

class Index extends Component {
  static propTypes = {
    device: PropTypes.string.isRequired,
  };
  static async getInitialProps({ req }) {
    const {
      device: { type },
    } = req;
    return { device: type };
  }
  render() {
    const { device } = this.props;
    return (
      <Landing device={device}>
        <Hero />
        <Community />
        <Work />
        <Contact />
        <Fun />
      </Landing>
    );
  }
}

export default Index;
