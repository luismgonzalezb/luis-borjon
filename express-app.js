const helmet = require('helmet');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const device = require('express-device');

const expressApp = express();

expressApp.use(bodyParser.json());
expressApp.use(device.capture());
expressApp.use(cookieParser());
expressApp.use(
  session({
    secret: 'tlespolcuatlo',
    // if you do SSL outside of node.
    proxy: false,
    // we support the touch method so per the
    // express-session docs this should be set to false
    resave: false,
    saveUninitialized: true,
  }),
);
expressApp.use(helmet());

module.exports = expressApp;
